extends Control

var simpleNode: Resource = load("res://SimpleNode.tscn")

func save_graph(graph:GraphEdit,path:String):
	var graph_data = GraphData.new()
	graph_data.connections = graph.get_connection_list()
	for node in graph.get_children():
		if(node is GraphNode):
			var node_data = NodeData.new()
			node_data.name = node.name
			node_data.offset = node.offset
			node_data.line = node.get_dialogue()
			for choice in node.get_choices():
				node_data.choices.append(choice.get_choice_text())
			graph_data.nodes.append(node_data)
	
	if ResourceSaver.save(path,graph_data) == OK:
		print("saved")
	else:
		print("error saving graph")

func load_graph(graph:GraphEdit,path:String):
	if(ResourceLoader.exists(path)):
		var graph_data = ResourceLoader.load(path)
		if(graph_data is GraphData):
			_init_graph(graph,graph_data)

func _init_graph(graph:GraphEdit, graph_data:GraphData):
	for node_data in graph_data.nodes:
		self.get_parent().load_node(node_data.offset,node_data.name,node_data.line,node_data.choices)

	for conn in graph_data.connections:
		var _e = graph.connect_node(conn.from,conn.from_port,conn.to,conn.to_port)
