extends Control

var simpleNode: Resource = load("res://SimpleNode.tscn")
var node_index:int = 0

# ejecutado cuando se presiona el boton de añadir nodo
func _on_AddButton_pressed():
	_add_node(Vector2(40,40) + node_index*Vector2(30,30))

# ejecutado cuando se presiona el boton de guardar el grafo
func _on_SaveButton_pressed():
	$SaveGraphDialog.popup()

# ejecutado cuando se presiona el boton de cargar el grafo
func _on_LoadButton_pressed():
	$LoadGraphDialog.popup()
	
# ejecuado cuando se presiona el boton para guardar el grafo como json
func _on_ExportJson_pressed():
	$ExportJsonDialog.popup()

# boton para testeos
func _on_Button_pressed():
	pass

# ejecutado cuando se trata de conectar un nodo con otro
func _on_GraphEdit_connection_request(from:String, from_slot:int, to:String, to_slot:int):
	for con in $GraphEdit.get_connection_list():
		if (con.to == to and con.to_port == to_slot):
			return
	$GraphEdit.connect_node(from,from_slot,to,to_slot)

# ejecutado cuando se conecta un nodo con un espacio vacio
func _on_GraphEdit_connection_to_empty(from:String, from_slot:int, release_position:Vector2):
	for con in $GraphEdit.get_connection_list():
		if(con.from == from and con.from_port == from_slot):
			return
		
	var newNode = _add_node(release_position)
	$GraphEdit.connect_node(from,from_slot,newNode.name,0)

# ejecutado cuando el user rompe una conexion entre nodos
func _on_GraphEdit_disconnection_request(from:String, from_slot:int, to:String, to_slot:int):
	$GraphEdit.disconnect_node(from,from_slot,to,to_slot)

# [b]Añade un nodo en una posicion del grafo indicada.[/b]
# [i]position: posicion donde spawnear el nodo[/i]
# [i]nodeName: nombre opcional para el nodo[/i]
func _add_node(position:Vector2,nodeName=""):
	var node = simpleNode.instance()
	node_index+=1
	var _finalName = ""
	if(nodeName == ""):
		_finalName = "node"+str(node_index)
	else:
		_finalName = nodeName
	node.offset = position
	node.title = _finalName
	$GraphEdit.add_child(node)
	node.name = _finalName
	node.connect("on_delete_choice",self,"_handle_delete_choice")
	node.connect("on_delete_node",self,"_handle_delete_node")

	return node

# [b]Carga un nodo junto con sus choices.[/b]
# [i]offset: posicion del nodo[/i]
# [i]name: nombre del nodo[/i]
# [i]line: linea de dialogo del nodo[/i]
# [i]choices: lista de choices del nodo[/i]
func load_node(offset:Vector2,name:String,line:String,choices:Array):
	var node = _add_node(offset,name)
	node.set_dialogue(line)
	for choice in choices:
		node.add_choice(choice)

# [b]Elimina todos los nodos del grafo.[/b]
func _clear_graph():
	$GraphEdit.clear_connections()
	
	var nodes = $GraphEdit.get_children()
	for node in nodes:
		if node is GraphNode:
			$GraphEdit.remove_child(node)
			node.queue_free()

# [b]Remueve una opcion de un nodo.[/b]
# Remueve todas las conexiones y luego reconecta las que no deberian haber sido borradas. Es medio sucio pero funciona.
# [i]node: nombre del nodo[/i]
# [i]index: index de la opcion[/i]
func _handle_delete_choice(node:String,index):
	var connections = $GraphEdit.get_connection_list()
	var _new_connections = []

	# desconecto todos las conexiones al nodo y guardo aquellas que no correspondan a la seleccionada
	for conn in connections:
		if(conn.from == node and conn.from_port!=index):
			_new_connections.append(conn)
		$GraphEdit.disconnect_node(conn.from,conn.from_port,conn.to,conn.to_port)

	# reconecto las conexiones no seleccionadas
	for ncon in _new_connections:
		var nindex = ncon.from_port - 1 if index < ncon.from_port else ncon.from_port
		$GraphEdit.connect_node(ncon.from,nindex,ncon.to,ncon.to_port)

# [b]Elimina un nodo del grafo.[/b]
# [i]name: nombre del nodo[/i]
func _handle_delete_node(name:String):
	_on_GraphEdit_delete_nodes_request([name])

# [b]Elimina una serie de nodos del grafo.[/b]
# [i]nodes: lista de nodos a eliminar[/i]
func _on_GraphEdit_delete_nodes_request(nodes:Array):
	var connections = $GraphEdit.get_connection_list()
	for node in nodes:
		for conn in connections:
			if(conn.from == node or conn.to == node):
				$GraphEdit.disconnect_node(conn.from,conn.from_port,conn.to,conn.to_port)
		_get_node_by_name(node).queue_free()

# [b]Retorna el nodo objeto, dado su nombre.[/b]
# [i]name: Nombre del nodo a buscar[/i]
func _get_node_by_name(name:String) -> Node:
	var nodes = $GraphEdit.get_children()
	for node in nodes:
		if(node.name == name):
			return node
	return null

func _on_ExportJsonDialog_file_selected(path:String):
	$ExportManager.export_graph_to_json($GraphEdit,path)


func _on_LoadGraphDialog_file_selected(path:String):
	_clear_graph()
	$SaveManager.load_graph($GraphEdit,path)
	
func _on_SaveGraphDialog_file_selected(path:String):
	$SaveManager.save_graph($GraphEdit,path)
