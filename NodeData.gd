extends Resource

class_name NodeData

export var name: String
export var line: String
export var offset: Vector2
export var choices: Array