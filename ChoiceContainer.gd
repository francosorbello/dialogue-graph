extends HBoxContainer

signal on_delete_choice(choice)

func _ready():
	pass 
	
func _on_Button_pressed():
	emit_signal("on_delete_choice",self)

# Retorna la linea de dialogo escrita en la opcion
func get_choice_text() -> String:
	return $TextEdit.text

# Setea la linea de dialogo para una opcion
func set_choice_text(line:String) -> void:
	$TextEdit.text = line
