extends GraphNode

signal on_delete_choice(node,index)
signal on_delete_node(node_name)

var _choice_container:Resource = load("res://ChoiceContainer.tscn")
var port_index:int=4
var _choices=[]
# Called when the node enters the scene tree for the first time.
func _ready():
	set_slot(0,true,0,Color(1,1,1,1),false,0,Color(0,1,0,1))

# Retorna el dialogo almacenado en el nodo
func get_dialogue() -> String:
	return $TextEdit.text

# Setea la linea de dialogo del nodo
func set_dialogue(line:String) -> void:
	$TextEdit.text = line

#retorna las choices del nodo:
func get_choices() -> Array:
	return _choices

func _on_GraphNode_close_request():
	# queue_free()
	emit_signal("on_delete_node",self.name)


func _on_GraphNode_resize_request(new_minsize:Vector2):
	rect_size = new_minsize

func _on_ChoiceButton_pressed():
	add_choice()

func add_choice(line:String = ""):
	var choice = _choice_container.instance()
	_choices.append(choice)
	choice.set_choice_text(line)
	choice.connect("on_delete_choice",self,"handle_delete_choice")
	add_child(choice)
	set_slot(port_index,false,0,Color(1,1,1,1),true,0,Color(0,1,0,1))
	port_index+=1

func handle_delete_choice(_choice):
	var index = _choices.find(_choice,0)
	assert(index != -1, "choice not found")
	emit_signal("on_delete_choice",self.name,index)

	_choices.remove(index)
	_choice.queue_free()