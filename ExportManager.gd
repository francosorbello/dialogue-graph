extends Control

func export_graph_to_json(graph:GraphEdit,path:String):
	var connections = graph.get_connection_list()
	var to_save = []
	for node in graph.get_children():
		if(node is GraphNode):
			var line = node.get_dialogue()
			var id = node.name
			var choices = _get_choices_from_connections(connections,node)
			
			var node_dict = {
				"id":id,
				"line":line,
				"choices":choices
			}
			to_save.append(node_dict)
			# print(node_dict)
	# print(to_save)
	_save_to_json(to_save,path)
	
func _save_to_json(nodes:Array,path:String):
	var savegame = File.new()
	savegame.open(path,File.WRITE)
	savegame.store_line("[")
	var i= 0
	for node in nodes:
		savegame.store_line(JSON.print(node,"\t"))
		if(i<len(nodes)-1):
			savegame.store_string(",")
		i+=1
	savegame.store_line("]")
	print("json saved")
	savegame.close()

func _get_choices_from_connections(connections:Array,node) -> Array:
	var final_choices = []
	var _related_connections = []
	#buscamos las conexiones del nodo
	for conn in connections:
		if(conn.from == node.name):
			_related_connections.append(conn)
	var choices = node.get_choices()
	for rel_con in _related_connections:
		# print([rel_con.to,choices[rel_con.from_port].get_choice_text()])
		final_choices.append({
			"line":choices[rel_con.from_port].get_choice_text(),
			"next":rel_con.to
		})
	return final_choices
